﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DebugSceneLoader : MonoBehaviour {

    public Dropdown m_sceneBuilderIndex;
    
	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this.gameObject);

        List<string> sceneNames = new List<string>();
        m_sceneBuilderIndex.ClearOptions();
        for (int i = 0; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            
            sceneNames.Add(""+i);

        }
        m_sceneBuilderIndex.AddOptions(sceneNames);
        m_sceneBuilderIndex.onValueChanged.AddListener(LoadScene);
	}

    private void LoadScene(int arg0)
    {
        foreach (NetworkManager netMan in FindObjectsOfType<NetworkManager>())
        {
            netMan.StopServer();
            netMan.StopMatchMaker();
            netMan.StopClient();
            netMan.StopHost();
            Destroy(netMan.gameObject);
        } 
        SceneManager.LoadScene( int.Parse(m_sceneBuilderIndex.options[arg0].text));
    }
    
}
