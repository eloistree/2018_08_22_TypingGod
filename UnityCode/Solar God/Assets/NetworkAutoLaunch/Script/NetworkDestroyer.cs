﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkDestroyer : MonoBehaviour {

	// Use this for initialization
	void Start () {

        NetworkAutoLauncher.KillAllNetworkManager();
        
	}
}
