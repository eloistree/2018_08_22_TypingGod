﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.Networking;

public class SyncPlayerInfo : NetworkBehaviour {

    [SyncVar]
    public string m_userName;
    [SyncVar]
    public string m_deviceName;
    [SyncVar]
    public string m_runningPlatform;
    [SyncVar]
    public string m_localIp;
    [SyncVar]
    public float m_jointNetworkTime;


    [Command]
    public void CmdUserIp(string ip) {
        m_localIp = ip;
    }

    public void SetPlatformInformatin(string deviceName, string platformName)
    {
        CmdSetPlatformInformatin(deviceName, platformName);
    }
    [Command]
    public void CmdSetPlatformInformatin(string deviceName, string platformName)
    {
        m_deviceName = deviceName;
        m_runningPlatform = platformName;

    }
    public void CmdSetJoiningTime(float time) {
        m_jointNetworkTime = time;
    }

    void Start () {
        if (isLocalPlayer && hasAuthority) {
            CmdUserIp(GetLocalIPAddress());
            CmdSetPlatformInformatin(SystemInfo.deviceName, Application.platform.ToString());
            CmdSetJoiningTime(Time.time);
            CmdSetName(NetworkAutoLauncher.GetUserName());
        }
	}
    [Command]
    private void CmdSetName(string name)
    {
        m_userName = name;
    }

    public static string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }
        throw new Exception("No network adapters with an IPv4 address in the system!");
    }

}
