﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetworkAutoLauncherAccess : MonoBehaviour {


    public void DestroyManagers() {
        NetworkAutoLauncher.KillAllNetworkManager();
    }

    public void LaunchUNetLanRoom() { }
    public void LaunchUNetLobbyRoom() { }

}
