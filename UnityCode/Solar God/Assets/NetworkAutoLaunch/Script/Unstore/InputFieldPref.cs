﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(InputField))]
public class InputFieldPref : MonoBehaviour {
    public string m_fieldId;
    public InputField m_field;
    // Use this for initialization
    private void Awake()
    {
        if(m_field==null)
            m_field = GetComponent<InputField>();
    }
    void Start () {
        m_field .text = PlayerPrefs.GetString("" + m_fieldId);
    }

    private void OnDestroy()
    {
        PlayerPrefs.SetString("" + m_fieldId, m_field.text);
    }

    public void Reset()
    {
        if (m_field == null)
            m_field = GetComponent<InputField>();
        m_fieldId =""+ Random.Range(0, int.MaxValue);
    }
}
