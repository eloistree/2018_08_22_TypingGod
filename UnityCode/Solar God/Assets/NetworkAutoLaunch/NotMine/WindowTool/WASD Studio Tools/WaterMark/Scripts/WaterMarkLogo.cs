﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public enum LogoPosition
{
	LEFT_TOP=0,
	RIGHT_TOP,
	RIGHT_BOTTOM,
	LEFT_BOTTOM,
	CENTER
}

[ExecuteInEditMode]
[AddComponentMenu("WASD Studio/Tools/WaterMark Logo")]
public class WaterMarkLogo : MonoBehaviour
{
	public bool autoShow=true;
	[HideInInspector]
	public Texture2D logo;
	[HideInInspector]
	public Color tintColor=Color.white;
	public LogoPosition position;
	[HideInInspector]
	public float normalizedLogoScale = 0.05f;
	[HideInInspector]
	public Vector2 offset;
	[Range(0,5f)]
	public float fadeInTime=0.5f;

	private bool show=false;
	private float timer;
	private Rect logoRect;

#if UNITY_EDITOR
	//add item to Create Menu
	[MenuItem("GameObject/Create Other/WASD Studio/Tools/WaterMark Logo")]
	static void CreateWaterMarkLogo()
	{
		GameObject WaterMark = AssetDatabase.LoadAssetAtPath("Assets/WASD Studio Tools/WaterMark/Prefabs/WASD Logo Watermark.prefab", typeof(GameObject)) as GameObject; 
		if (WaterMark!=null) 
		{
			GameObject myWaterMark = GameObject.Instantiate(WaterMark,Vector3.zero,Quaternion.identity) as GameObject;
			myWaterMark.transform.name = myWaterMark.transform.name.Remove(myWaterMark.transform.name.Length-7,7);
		}
		else
		{
			Debug.LogError("WASD Studio Tools/WASD Logo Watermark.prefab not found!");
		}
	}
#endif
	// Use this for initialization
	void Start () 
	{
		show = autoShow;
	}
	
	// Update is called once per frame
	void OnGUI () 
	{
		GUI.depth = -1;
		if (show)
		{

			if (Application.isPlaying)
			{
				GUI.color = Color.Lerp(new Color(tintColor.r,tintColor.g,tintColor.b,0),
		    	                                              new Color(tintColor.r,tintColor.g,tintColor.b,tintColor.a),
		        	                                          (Time.time-timer)/fadeInTime);
			}
			else
			{
				GUI.color = tintColor;
			}
			if (logo!=null) 
			{
				AdjustLogo();
				GUI.DrawTexture(logoRect,logo);
			}
		}
	}

	public void ShowWaterMark()
	{
		show = true;
		timer = Time.time;
	}

	public void HideWaterMark()
	{
		show = false;
	}

	public void ChangePosition(LogoPosition newPosition)
	{
		position = newPosition;
	}

	public void ChangeLogo(Texture2D newLogo)
	{
		logo = newLogo;
	}

	private void AdjustLogo()
	{

		float newLogoWidth = normalizedLogoScale * Screen.width;
		float newLogoHeight = (logo.height * newLogoWidth) / logo.width;
		switch (position)
		{
			case LogoPosition.LEFT_BOTTOM:
				logoRect = new Rect((offset.x * Screen.width),
			                    (Screen.height-newLogoHeight)-(offset.y * Screen.width),
			                    newLogoWidth,newLogoHeight);
				break;
			case LogoPosition.LEFT_TOP:
				logoRect = new Rect((offset.x * Screen.width),
			                    (offset.y * Screen.width),
			                    newLogoWidth,newLogoHeight);
				break;
			case LogoPosition.RIGHT_BOTTOM:
				logoRect = new Rect((Screen.width-newLogoWidth)-(offset.x * Screen.width),
			                    (Screen.height-newLogoHeight)-(offset.y * Screen.width),
			                    newLogoWidth,newLogoHeight);
				break;
			case LogoPosition.RIGHT_TOP:
			logoRect = new Rect((Screen.width-newLogoWidth)-(offset.x * Screen.width),
			                    (offset.y * Screen.width),
			                    newLogoWidth,newLogoHeight);
				break;
			case LogoPosition.CENTER:
				logoRect = new Rect((Screen.width * 0.5f)-(newLogoWidth * 0.5f),
			                    (Screen.height * 0.5f)-(newLogoHeight * 0.5f),
			                    newLogoWidth,newLogoHeight);
				break;
		}
	}
}
