﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeLord : MonoBehaviour {

    public float m_speed=0.5f;

    private void Update()
    {
        Time.timeScale = m_speed;
    }
    
}
