﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetTimeFromAudioSource : GetTimeFrom {

    public AudioSource m_audioSource;

    public float GetMaxTime() { return m_audioSource.clip.length; }

    public override float GetPourcent()
    {
        return GetCurrentSecondInAudio() / GetMaxTime();
    }

    public override float GetTime()
    {
        return GetCurrentSecondInAudio() ;
    }

    private float GetCurrentSecondInAudio()
    {
        if (m_audioSource == null || m_audioSource.clip == null)
            return 0;
        return (float)m_audioSource.timeSamples / (float)m_audioSource.clip.frequency;
    }
}
