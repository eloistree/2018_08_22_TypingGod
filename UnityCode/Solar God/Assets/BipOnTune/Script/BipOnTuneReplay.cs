﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public  class BipOnTuneReplay : MonoBehaviour {

    public BipOnTune m_bipOnTune;
    public GetTimeFrom m_time;
    public OnKeyFrameEvent m_onKeyFrame;
    [System.Serializable]
    public class OnKeyFrameEvent : UnityEvent<TuneKeyFrame> { }

    [Header("Debug")]
    public float m_lastTime;
    public float m_currentTime;


  
    void Update()
    {
        m_currentTime = GetTime();

        if (m_currentTime!=0 &&  m_currentTime < m_lastTime)
            m_lastTime = m_currentTime;

        ////////////

        List<TuneKeyFrame> actions = m_bipOnTune.GetActionsToDo(m_lastTime, m_currentTime);
        foreach (TuneKeyFrame action in actions)
        {
            action.DoTheThing();
            m_onKeyFrame.Invoke(action);
        }


        /////////////
        m_lastTime = m_currentTime;
    }

    public float GetTime() { return m_time.GetTime(); }

    private void Reset()
    {
        if (m_bipOnTune == null)
            m_bipOnTune = GetComponent<BipOnTune>();
        if (m_bipOnTune == null)
            m_bipOnTune = GetComponentInParent<BipOnTune>();
    }

    internal TuneKeyFrame[] GetFrames()
    {
        return m_bipOnTune.GetActionsToDo(0, m_currentTime).ToArray();
    }
    internal String[] GetFramesDescription()
    {
        return m_bipOnTune.GetActionsDescriptiono(0, m_currentTime).ToArray();
    }
    internal String[] GetFramesDescription(float pct)
    {
        return m_bipOnTune.GetActionsDescriptiono(0, pct).ToArray();
    }
    
}
