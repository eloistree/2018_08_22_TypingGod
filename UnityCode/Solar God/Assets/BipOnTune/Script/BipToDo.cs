﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BipToDo : MonoBehaviour
{

    public TuneKeyFrame m_keyFrame;
    public int m_randomId=-1;


    public void Awake()
    {

    }

    private void OnDestroy()
    {
        Export();
    }
    private void OnValidate()
    {
        GetRandomId();
        
        if(!Application.isPlaying)
          Import();
    }
    private void Reset()
    {
        GetRandomId();
    }
        private void GetRandomId()
    {
        if (m_randomId < 0)
            m_randomId = UnityEngine.Random.Range(0, int.MaxValue);
    }

    public void Export()
    {
        PlayerPrefs.SetFloat(m_randomId.ToString(), m_keyFrame.m_time);

    }
    public void Import()
    {

        m_keyFrame.m_time= PlayerPrefs.GetFloat(m_randomId.ToString());
    }
}
