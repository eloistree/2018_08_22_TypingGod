﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AutoLoadScene : MonoBehaviour {

    public string m_sceneName;
    public float m_countDown = 3f;

    void Update () {
       
        m_countDown -= Time.deltaTime;
        if (m_countDown < 0) {
            if (string.IsNullOrEmpty(m_sceneName ))
            {
                int currentScene = SceneManager.GetActiveScene().buildIndex;
                //m_sceneName =  //SceneManager.GetSceneByBuildIndex(currentScene + 1).name;
                Application.LoadLevel(Application.loadedLevel + 1);
            }
            else
                SceneManager.LoadScene(m_sceneName);
            //Destroy(this);
        }
	}
}
