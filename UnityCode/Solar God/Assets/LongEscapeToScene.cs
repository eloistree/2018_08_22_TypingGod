﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LongEscapeToScene : MonoBehaviour {

    public string m_sceneName="";
    public float m_timeToActivate=2f;
    public float m_time;
    public bool m_dontDestroy=true;
	void Start () {
        if (m_dontDestroy)
            DontDestroyOnLoad(this);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Escape))
        {
            m_time += Time.deltaTime;
            if (m_time > m_timeToActivate)
                SceneManager.LoadScene(m_sceneName);
        }
        else m_time = 0;
	}
}


public class LetterPoint {

    public char m_character;
    public float m_perfectTiming;
    public float m_maxValidation;
}