﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListenToTypedWord : MonoBehaviour {
    
    public KillTheLyrics m_killTheLyrics;
    public string m_constructWord;

    public Text m_debug;
	// Use this for initialization
	void Start () {
		
	}
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
            TryToPushWord();

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            if(m_constructWord.Length>1)
                m_constructWord= m_constructWord.Substring(0, m_constructWord.Length - 2);
        }
        Refresh();
    }
    
    // Update is called once per frame
    public void AddCharacter (char c) {
        if (c == ' ')
        {
            TryToPushWord();
        }
        else {

            m_constructWord += c;
        }

    }

    private void Refresh()
    {
        if(m_debug)
            m_debug.text = m_constructWord;
    }

    private void TryToPushWord()
    {
        if (m_constructWord.Length > 0)
        {

            m_killTheLyrics.TryToKillLyrics(m_constructWord);
            m_constructWord = "";
        }
    }
}
