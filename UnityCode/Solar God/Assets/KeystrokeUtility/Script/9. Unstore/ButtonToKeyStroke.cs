﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonToKeyStroke : MonoBehaviour {

    public KeyboardTouch m_touch;

    public void SendTouchToOtherDevices()
    {
        KeystrokeUtility.Network.PressTouch(m_touch);
    }
}
