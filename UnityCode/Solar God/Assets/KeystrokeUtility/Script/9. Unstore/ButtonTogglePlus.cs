﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonTogglePlus : MonoBehaviour {

    public Button m_unityButton;
    public bool m_state;
    public Color m_colorOn= Color.green;
    public Color m_colorOff= Color.white;

    void Start () {
        Refresh();
	}
    public void Switch() {
        m_state = !m_state;
        Refresh();
    }
    private void Refresh()
    {
        m_unityButton.image.color = m_state ? m_colorOn : m_colorOff;
    }
    
}
