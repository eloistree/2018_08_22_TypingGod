﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_KeyboardStateViewer : MonoBehaviour {

    public KeyboardState m_keyboard;
    public Text m_information;
    public Toggle m_capsLockOn;
    public Toggle m_numLockOn;
    //public Toggle m_scrollLockOn;
    public Toggle m_shiftDown;
    public Toggle m_ctrlDown;
    public Toggle m_metaDown;
    public Toggle m_altDown;
    public Toggle m_altGrDown;

    public void SetKeyboard(KeyboardState keyboard) {
        m_keyboard = keyboard;
    }
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
