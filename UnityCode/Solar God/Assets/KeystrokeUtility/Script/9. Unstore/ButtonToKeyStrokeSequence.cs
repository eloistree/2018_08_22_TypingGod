﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonToKeyStrokeSequence : MonoBehaviour {

    public KeyboardTouch[] m_touches;
    public float m_timeBetween=0.1f;

    public void SendTouchesToOtherDevices() {

        StartCoroutine(StartStroking());
    }

    private IEnumerator StartStroking()
    {
        for (int i = 0; i < m_touches.Length; i++)
        {
            SendTouchToOtherDevices( m_touches[i] );
            yield return new WaitForSeconds(m_timeBetween);

        }
    }

    public void SendTouchToOtherDevices(KeyboardTouch touch)
    {
        KeystrokeUtility.Network.PressTouch(touch);
    }
}
