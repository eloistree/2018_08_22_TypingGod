﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_SwitchTransmissionType : MonoBehaviour {

	// Use this for initialization
	public void SwitchTransmissionType (int type) {
        TransmissionType t;
        switch (type)
        {
            case 0: t = TransmissionType.Receptor; break;
            case 1: t = TransmissionType.Transmitter; break;
            default:
            case 2:
                t = TransmissionType.Both; break;
        }
        KeystrokeUtility.Network.SwitchTransmissionTo(t);	
	}
	
}
