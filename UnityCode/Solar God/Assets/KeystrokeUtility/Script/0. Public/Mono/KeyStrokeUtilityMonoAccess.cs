﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput.Native;

public class KeyStrokeUtilityMonoAccess : MonoBehaviour {


    public void NotifyNativeChar(char value)
    { KeystrokeUtility.NotifyNativeStroke(value); }
    public void NotifyNativeText(string value)
    { KeystrokeUtility.NotifyNativeStroke(value); }


    public void NotifyNativeUnityKeyCodeDown(KeyCode value) {
        NotifyNativeUnityKeyCode(value, true); }
    public void NotifyNativeUnityKeyCodeUp(KeyCode value)
    {NotifyNativeUnityKeyCode(value, false);}
    public void NotifyNativeUnityKeyCode(KeyCode value, bool isDown)
    { KeystrokeUtility.NotifyNativePress(value, isDown); }

    public void NotifyNativeWindowKeyCodeDown(VirtualKeyCode value)
    {NotifyNativeWindowKeyCode(value, true); }
    public void NotifyNativeWindowKeyCodeUp(VirtualKeyCode value)
    { NotifyNativeWindowKeyCode(value, false); }
    public void NotifyNativeWindowKeyCode(VirtualKeyCode value, bool isDown)
    { KeystrokeUtility.NotifyNativePress(value, isDown); }


    public void PushTextToNetwork(string value)
    {
        KeystrokeUtility.Network.StrokeText(value);
    }
    public void PushCharacterToNetwork(char value)
    {
        KeystrokeUtility.Network.StrokeCharacter(value);
    }

    public void PushDownTouchToNetwork(KeyboardTouch value)
    {
        KeystrokeUtility.Network.PressDownTouch(value);
    }
    public void PushUpTouchToNetwork(KeyboardTouch value)
    {
        KeystrokeUtility.Network.PressUpTouch(value);
    }


    private void NotifyText(TextStrokeRequest value)
    {
        throw new System.NotImplementedException();
    }
    private void NotifyCharacter(CharacterStrokeRequest value)
    {
        throw new System.NotImplementedException();
    }
    private void NotifyCharUp(KeyboardCharPressRequest value)
    {
        throw new System.NotImplementedException();

    }
    private void NotifyCharDown(KeyboardCharPressRequest value)
    {
        throw new System.NotImplementedException();
    }
    private void NotifyUnicode(UnicodeStrokeRequest value)
    {
        throw new System.NotImplementedException();
    }
    private void NotifyASCII(AsciiStrokeRequest value)
    {
        throw new System.NotImplementedException();
    }
    private void NotifyKeyUp(KeyboardTouchPressRequest value)
    {
        throw new System.NotImplementedException();
    }
    private void NotifyKeyDown(KeyboardTouchPressRequest value)
    {
        throw new System.NotImplementedException();
    }

    public void SwitchDeviceAsTransmitter()
    {

        KeystrokeUtility.Network.SwitchTransmissionTo(TransmissionType.Transmitter);
    }
    public void SwitchDeviceAsReceptor()
    {

        KeystrokeUtility.Network.SwitchTransmissionTo(TransmissionType.Receptor);
    }

    

   public void ReconnectToNetwork(KeystrokeNetworkType networkType) {
        KeystrokeUtility.Network.ReconnectToNetwork(networkType);
    }
    public void ReconnnectToUnityLocalNetwork()
    {
        ReconnectToNetwork(KeystrokeNetworkType.UNet_Local);
    }
    public void ReconnnectToUnityLobbyNetwork()
    {
        ReconnectToNetwork(KeystrokeNetworkType.UNet_Lobby);
    }
    public void ReconnectToCurrentNetwork() {
        KeystrokeUtility.Network.ReconnectToCurrentNetwork();
    }

    public void SetCurrentNetwork(KeystrokeNetworkType networkType, string roomName, string userName) {

        KeystrokeUtility.Network.Info.SetWantedNetwork(networkType);
        KeystrokeUtility.Network.Info.SetRoomName(roomName);
        KeystrokeUtility.Network.Info.SetUserName(userName);
    }
}
