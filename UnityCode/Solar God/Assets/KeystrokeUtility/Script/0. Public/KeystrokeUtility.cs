﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using WindowsInput.Native;
public class KeystrokeUtility  {

    public static GameObject m_defaultKeyUtilityInScene;
    public static void InvokeDefaultListener()
    {
        if (m_defaultKeyUtilityInScene == null) {
            m_defaultKeyUtilityInScene = GameObject.Instantiate(Resources.Load("KeystrokeUtilitySingleton", typeof(GameObject))) as GameObject;
            m_defaultKeyUtilityInScene.name = "#KeystrokeUtilitySingleton";
        }
    }

    private static void CheckSingleton()
    {
        InvokeDefaultListener();
    }


    #region LISTEN TO NATIVE
    public delegate void OnNativeCharacter(char value);
    public delegate void OnNativeString(string value);
    public delegate void OnNativeUnityKeyCode(KeyCode value, bool isDown);
    public delegate void OnNativeWindowKeyCode(VirtualKeyCode value, bool isDown);

    internal static OnNativeCharacter onNativeCharacter;
    internal static OnNativeString onNativeText;
    internal static OnNativeUnityKeyCode onNativeUnityKeyCode;
    internal static OnNativeWindowKeyCode onNativeWindowKeyCode;

    public static void NotifyNativeStroke(char value) { if (onNativeCharacter != null) { onNativeCharacter(value); } }
    public static void NotifyNativeStroke(string value) { if (onNativeText != null) { onNativeText(value); } }
    public static void NotifyNativePress(KeyCode value, bool isDown) { if (onNativeUnityKeyCode != null) { onNativeUnityKeyCode(value,isDown); } }
    public static void NotifyNativePress(VirtualKeyCode value, bool isDown) { if (onNativeWindowKeyCode != null) { onNativeWindowKeyCode(value,isDown); } }
    #endregion



    #region LISTEN TO KEYSTROKE
    public delegate void OnKeyboardTouchRequest(KeyboardTouchPressRequest touch);
    public delegate void OnKeyboardCharRequest(KeyboardCharPressRequest touch);
    public delegate void OnStrokeCharacter(CharacterStrokeRequest character);
    public delegate void OnStrokeText(TextStrokeRequest text);
    public delegate void OnStrokeCharacter<T>(T codeNumber) where T : StrokeRequest;

    public delegate void OnKeyboardNetworkTouchRequest(NetworkTransmissionInfo fromTo, KeyboardTouchPressRequest touch);
    public delegate void OnKeyboardNetworkCharRequest(NetworkTransmissionInfo fromTo, KeyboardCharPressRequest touch);
    public delegate void OnStrokeNetworkCharacter(NetworkTransmissionInfo fromTo, CharacterStrokeRequest character);
    public delegate void OnStrokeNetworkText(NetworkTransmissionInfo fromTo, TextStrokeRequest text);
    public delegate void OnStrokeNetworkCharacter<T>(NetworkTransmissionInfo fromTo, T codeNumber) where T : StrokeRequest;

    public static OnKeyboardTouchRequest onKeyDown;
    public static OnKeyboardTouchRequest onKeyUp;
    public static OnKeyboardCharRequest onCharDown;
    public static OnKeyboardCharRequest onCharUp;
    public static OnStrokeCharacter<AsciiStrokeRequest> onStrokeRequestASCII;
    public static OnStrokeCharacter<UnicodeStrokeRequest> onStrokeRequestUnicode;
    public static OnStrokeCharacter onStrokeRequestCharacter;
    public static OnStrokeText onStrokeRequestText;
    

    public static void PressKeyDown(KeyboardTouch key)
    {
        KeystrokeUtility.CheckSingleton();
        if (onKeyDown != null)
            onKeyDown(new KeyboardTouchPressRequest( key, PressType.Down));
    }
    public static void PressKeyUp(KeyboardTouch key)
    {
        KeystrokeUtility.CheckSingleton();
        if (onKeyUp != null)
            onKeyUp(new KeyboardTouchPressRequest(key, PressType.Up));

    }

    public static void PressKeyDown(char character)
    {
        KeystrokeUtility.CheckSingleton();
        if (onCharDown != null)
            onCharDown(new KeyboardCharPressRequest(character, PressType.Down));
    }
    public static void PressKeyUp(char character)
    {
        KeystrokeUtility.CheckSingleton();
        if (onCharUp != null)
            onCharUp(new KeyboardCharPressRequest(character, PressType.Up));

    }

    internal static void StrokeCharacter(char character)
    {
        CheckSingleton();
        if (onStrokeRequestCharacter != null)
            onStrokeRequestCharacter(new CharacterStrokeRequest(character));
    }
    
    internal static void StrokeText(string text)
    {
        CheckSingleton();
        if (onStrokeRequestText != null)
            onStrokeRequestText(new TextStrokeRequest(text));
    }


    /// <summary>
    /// ALT + Keypad (int)
    /// </summary>
    /// <param name="asciiCode"></param>
    public static void StrokeASCII(int asciiCode)
    {
        CheckSingleton();
        if (onStrokeRequestASCII!=null)
            onStrokeRequestASCII(new AsciiStrokeRequest(asciiCode));

    }
    public static void StrokeASCII(char character)
    {
        CheckSingleton();
        bool isConvertable;
        int asciiCode;
        KeyBindingTable.ConvertCharToASCII(character, out asciiCode, out isConvertable);

        if(isConvertable)
            StrokeASCII(asciiCode);

    }
    public static bool IsASCII(char character) {
        return KeyBindingTable.IsCharASCII(character);
    }

    /// <summary>
    /// ALT + x + Keypad (int)
    /// </summary>
    /// <param name="unicode"></param>
    public static void StrokeUnicode(int unicode)
    {
        CheckSingleton();
        if (onStrokeRequestUnicode != null)
            onStrokeRequestUnicode(new UnicodeStrokeRequest(unicode));
    }
    public static void StrokeUnicode(char character)
    {
        CheckSingleton();
        bool isConvertable;
        int unicode;
        KeyBindingTable.ConvertCharToUnicode(character, out unicode, out isConvertable);
        if (isConvertable)
            StrokeUnicode(unicode);
    }
    #endregion



    #region Access LIST OF 
    #region SEALED
    public static List<KeyCode> GetUnityKeyCodes()
    { return GetEnumList<KeyCode>(); }


    public static List<VirtualKeyCode> GetVirtualKeyCode()
    { return GetEnumList<VirtualKeyCode>(); }

    public static char[] GetAllASCII()
    { return KeyBindingTable.GetAllASCII(); }
    #endregion

    #region CAN CHANGED
    public static List<KeyboardTouch> GetKeyTouchs()
    { return GetEnumList<KeyboardTouch>(); }
    #endregion
    #endregion
    public static List<T> GetEnumList<T>() where T : struct, IConvertible
    {
        return Enum.GetValues(typeof(T)).OfType<T>().ToList();
    }
    public static List<string> GetEnumNamesList<T>() where T : struct, IConvertible
    {
        return Enum.GetNames(typeof(T)).ToList();
    }
    public static class Keyboards
    {
        public static KeyboardState m_representative;
        public static void SetKeyboardsAsRepresentative(KeyboardState keyboard) {
            m_representative = keyboard;
        }
        public static KeyboardState [] GetAllKeyboards() {
            MonoBehaviour [] objs = GameObject.FindObjectsOfType<MonoBehaviour>();


            return objs.OfType<KeyboardState>().ToArray();
        }
        public static KeyboardState GetKeyboardBasedOnPlatform(RuntimePlatform platform)
        {
            KeyboardPlatform keyboardPlatform = KeyboardPlatform.Unknow;
            switch (platform)
            {
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.OSXPlayer:
                    keyboardPlatform = KeyboardPlatform.Mac;
                    break;

                case RuntimePlatform.WindowsPlayer:
                case RuntimePlatform.WindowsEditor:
                    keyboardPlatform = KeyboardPlatform.Window;
                    break;

                case RuntimePlatform.Android:
                    keyboardPlatform = KeyboardPlatform.Android;
                    break;

                case RuntimePlatform.LinuxPlayer:
                case RuntimePlatform.LinuxEditor:
                    keyboardPlatform = KeyboardPlatform.Linux;
                    break;
                    
                default:
                    break;
            }
            return GetKeyboardBasedOnPlatform(keyboardPlatform);
        }

        public static KeyboardState GetKeyboardBasedOnPlatform(KeyboardPlatform platform)
        {
            KeyboardState [] keyboards = GetAllKeyboards();
            keyboards = keyboards.Where(k => k.GetRepresentedPlatform() == platform).ToArray();
            if (keyboards.Length > 0)
                return keyboards[0];
            return null;
        }

        internal static KeyboardState GetRepresentativeKeyboard()
        {
            CheckSingleton();
            return m_representative;
        }
    }
    public static class Network
    {

        public static OnKeyboardNetworkTouchRequest onKeyDown;
        public static OnKeyboardNetworkTouchRequest onKeyUp;
        //public static OnKeyboardCharRequest onCharDown;
        //public static OnKeyboardCharRequest onCharUp;
        public static OnStrokeNetworkCharacter onStrokeRequestCharacter;
        public static OnStrokeNetworkText onStrokeRequestText;

        internal static void PressDownTouch(KeyboardTouch touch)
        {
            KeystrokeUtility.CheckSingleton();
            PressUpTouch(touch, true);
        }

        internal static void PressUpTouch(KeyboardTouch touch)
        {
            KeystrokeUtility.CheckSingleton();
            PressUpTouch(touch, false);
        }
        internal static void PressUpTouch(KeyboardTouch touch, bool isDown)
        {
            KeystrokeUtility.CheckSingleton();
            if (!CheckForTransmittor()) {
                return;
            }
            Network.Transmitter.PressTouch(touch, isDown);
        }
        internal static void StrokeCharacter(char c)
        {
            KeystrokeUtility.CheckSingleton();
            if (!CheckForTransmittor())
            {
                return;
            }
            Network.Transmitter.StrokeCharacter(c);

        }
        internal static void StrokeText(string text)
        {
            KeystrokeUtility.CheckSingleton();
            if (!CheckForTransmittor())
            {
                return;
            }
            Network.Transmitter.StrokeText(text);

        }


        private static KeystrokeNetworkBehaviour m_transmittorInScene;
        public static bool CheckForTransmittor() {
            bool mainNetworkFound = m_transmittorInScene != null;
            if (m_transmittorInScene == null)
            {
                Debug.Log("Look for Transmittor");
                    NetworkIdentity user = null;
                    NetworkIdentity[] users = GameObject.FindObjectsOfType<NetworkIdentity>();

                    if (users.Length <= 0)
                    {
                        Debug.Log("Not network users");
                    }
                    else
                    {
                        user = users.Where(k => k.hasAuthority && k.isLocalPlayer).First();
                        if (user == null)
                            Debug.Log("Not network main user found");
                        else
                        {
                            Debug.Log("User Found", user.gameObject);
                            m_transmittorInScene = user.GetComponent<KeystrokeNetworkBehaviour>();
                            mainNetworkFound = true;
                        }

                    }
                }

            return mainNetworkFound;
        }

        internal static bool IsTransmitter()
        {
            return m_transmitionType == TransmissionType.Transmitter || m_transmitionType == TransmissionType.Both;
        }

        internal static bool IsReceptor()
        {

            return m_transmitionType == TransmissionType.Receptor || m_transmitionType == TransmissionType.Both;
        }
        public static void SwitchTransmissionTo(TransmissionType transmisson) {
            KeystrokeUtility.CheckSingleton();
            if (m_transmitionType != transmisson) {
                m_transmitionType = transmisson;
                if (onTransmissionChanged != null)
                    onTransmissionChanged(m_transmitionType);

            }
        }
        private static TransmissionType m_transmitionType = TransmissionType.Receptor;
        public static OnTransmissionmChanged onTransmissionChanged;
        public delegate void OnTransmissionmChanged(TransmissionType type);
    

        public static KeystrokeNetworkBehaviour Transmitter { get {
                KeystrokeUtility.CheckSingleton();
                CheckForTransmittor();
                return m_transmittorInScene;
            }
        }
        

        public static KeystrokeNetworkBehaviour[] GetTransmitters() {
            return GameObject.FindObjectsOfType<KeystrokeNetworkBehaviour>();
        }

        internal static string[] GetUsersId()
        {
           return  NetworkUsersName.GetUserNames().ToArray();
        }

        internal static KeyboardState GetUserPlatformKeyboardState(string userName)
        {
            NetworkUsersName user = NetworkUsersName.GetUserFromName(userName);
            NetworkKeyboardState platformInfo = user.GetComponent<NetworkKeyboardState>();
            return platformInfo.m_trackedKeyboard;
        }

        internal static void PressTouch(KeyboardTouch touch)
        {
            PressDownTouch(touch);
            PressUpTouch(touch);

        }


        public static class Info
        {
            private static KeystrokeNetworkType m_wantedNetworkType = KeystrokeNetworkType.UNet_Local;
            private static string m_roomName="Default";
            private static string m_roomEnterPassword = "Admin";
            private static string m_userName = "Unnamed";

            public static void SetWantedNetwork(KeystrokeNetworkType type) {
                m_wantedNetworkType = type;
            }

            internal static void SetRoomName(string roomName)
            {
                m_roomName = roomName;
            }

            internal static void SetUserName(string userName)
            {
                m_userName = userName;
            }

            internal static void SetRoomPassword(string password)
            {
                m_roomEnterPassword = password;
            }

            public static KeystrokeNetworkType GetWantedNetworkType() { return m_wantedNetworkType; }
            public static string GetWantedUserName() { return m_userName; }
            public static string GetWantedRoomName() { return m_roomName; }
            public static string GetWantedRoomPassword() { return m_roomEnterPassword; }

        }

        public static OnReconnectToNetworkRequested onReconnectNetworkRequest;
        public delegate void OnReconnectToNetworkRequested(KeystrokeNetworkType networkType, string roomName);

        public static void ReconnectToNetwork(KeystrokeNetworkType networkType)
        {
            if (onReconnectNetworkRequest != null) {
                onReconnectNetworkRequest(Info.GetWantedNetworkType(), Info.GetWantedRoomName());
            }
        }
        public static void ReconnnectToUnityLocalNetwork()
        {
            ReconnectToNetwork(KeystrokeNetworkType.UNet_Local);
        }
        public static void ReconnnectToUnityLobbyNetwork()
        {
            ReconnectToNetwork(KeystrokeNetworkType.UNet_Lobby);
        }
        public static void ReconnectToCurrentNetwork()
        {
            ReconnectToNetwork(Info.GetWantedNetworkType());
        }

        public static void SetCurrentNetwork(KeystrokeNetworkType networkType, string roomName, string userName)
        {

            Info.SetWantedNetwork(networkType);
            Info.SetRoomName(roomName);
            Info.SetUserName(userName);
        }
    }
}
public enum TransmissionType
{
    Transmitter,
    Receptor,
    Both// Hight risk of Network Loop !!!
}


public interface KeyboardState
{
    bool IsTouchDown(KeyboardTouch keyboardTouch);
    bool IsTouchUp(KeyboardTouch keyboardTouch);
    bool IsControlDown();
    bool IsShiftDown();
    bool IsMetaDown();
    bool IsAltDown();
    bool IsAltGrDown();
    bool IsCapsLockOn();
    bool IsNumLockOn();
    bool IsScrollLockOn();
    KeyboardPlatform GetRepresentedPlatform();
    KeyboardTouch[] GetPressedTouches();

}


public enum KeyboardPlatform { Window, Mac, Linux, Android, DefaultUnity, Custom, Unknow}

public interface InteractableKeyboard {
    void RealPressDown(KeyboardTouch touch);
    void RealPressUp(KeyboardTouch touch);

    void Stroke(string text);
    void Stroke(char character);

}

public interface Keyboardable {
     void PressKey(KeyboardTouchPressRequest key);
     void PressKey(KeyboardCharPressRequest character);

    /// <summary>
    /// ALT + Keypad (int)
    /// </summary>
    /// <param name="asciiCode"></param>
     void StrokeASCII(AsciiStrokeRequest asciiCode);

    /// <summary>
    /// ALT + x + Keypad (int)
    /// </summary>
    /// <param name="unicode"></param>
      void StrokeUnicode(UnicodeStrokeRequest unicode);
}
public enum KeystrokeNetworkType { UNet_Local, UNet_Lobby } // Next TNet, OSC, UDP , UNet2, Photon
[System.Serializable]
public class NetworkTransmissionInfo
{
    public NetworkTransmissionInfo(string sender, params string[] targets)
    {
        m_sender = sender;
        m_targets = targets;
    }
    public string m_sender;
    public string[] m_targets;
}


#region DATA HOLDER
public enum PressType :int { Down, Up, Both }
[System.Serializable]
public class KeyboardTouchPressRequest
{
    public KeyboardTouchPressRequest(KeyboardTouch touch, PressType pression)
    {
        m_touch = touch;
        m_pression = pression;
    }
    public KeyboardTouch m_touch;
    public PressType m_pression;
}

/// <summary>
/// Correspond of combisation of Touch press depending of the target keyboard language
/// </summary>
[System.Serializable]

public class KeyboardCharPressRequest
{

    public KeyboardCharPressRequest(char character, PressType pression)
    {
        m_character = character;
        m_pression = pression;
    }
    public char m_character;
    public PressType m_pression;
}

[System.Serializable]
public class StrokeRequest
{
    public StrokeRequest(int code)
    {
        m_code = code;
    }
    public int m_code;
}

[System.Serializable]
public class UnicodeStrokeRequest : StrokeRequest
{
    public UnicodeStrokeRequest(int code): base(code)
    {
    }
}
[System.Serializable]
public class AsciiStrokeRequest : StrokeRequest
{
    public AsciiStrokeRequest(int code) : base(code)
    {
    }
}
[System.Serializable]
public class CharacterStrokeRequest
{
    public CharacterStrokeRequest(char character)
    {
        m_character = character;
    }
    public char m_character;
}
[System.Serializable]
public class TextStrokeRequest
{
    public TextStrokeRequest(string text)
    {
        m_text = text;
    }
    public string m_text;
}




#endregion
