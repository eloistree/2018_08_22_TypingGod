﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public  abstract class KeyboardKeyListener : MonoBehaviour, KeyboardState {


    public EnumDictionary<KeyboardTouch> m_keyboardsState;
    public List<KeyboardTouch> m_activeKeys;
    public List<KeyboardTouch> m_availaibleKeys;

    public OnKeyChange m_onKeyDown;
    public OnKeyChange m_onKeyUp;

    public bool m_isCapsLockOn;
    public bool m_isNumLockOn;
    public bool m_isScrollLock;

    [System.Serializable]
    public class OnKeyChange : UnityEvent<KeyboardTouch> {
    }
    

    protected virtual void Awake()
    {
        m_keyboardsState = new EnumDictionary<KeyboardTouch>();
        m_availaibleKeys = MacroUtility.GetEnumList<KeyboardTouch>();
        m_keyboardsState.onStateChange += NotifyEvent;

    } 

    protected virtual void Update()
    {
        m_activeKeys = m_keyboardsState.GetActiveElements();
        foreach (KeyboardTouch vkc in m_availaibleKeys)
        {
            m_keyboardsState.SetState(vkc, IsTouchDown(vkc));
        }
        m_isCapsLockOn = IsCapsLockOn();
        m_isNumLockOn = IsNumLockOn(); 
        m_isScrollLock = IsScrollLockOn(); 




    }
    private void NotifyEvent(KeyboardTouch element, bool isOn)
    {
        if (isOn)
            m_onKeyDown.Invoke(element);
        else
            m_onKeyUp.Invoke(element);
    }
  

    void OnValidate()
    {
        m_availaibleKeys = MacroUtility.GetEnumList<KeyboardTouch>();
    }

   
    public bool IsAltDown()
    {
        return m_keyboardsState.GetState(KeyboardTouch.Alt);
    }

    public bool IsAltGrDown()
    {
        return m_keyboardsState.GetState(KeyboardTouch.AltGr);
    }


    public bool IsControlDown()
    {
        return  m_keyboardsState.GetState(KeyboardTouch.Control);
    }

    public bool IsMetaDown()
    {
        return m_keyboardsState.GetState(KeyboardTouch.Meta);
    }
    public bool IsShiftDown()
    {
        return m_keyboardsState.GetState(KeyboardTouch.Shift);
    }



    public abstract bool IsCapsLockOn();
    public abstract bool IsNumLockOn();
    public abstract bool IsScrollLockOn();

  

    public abstract bool IsTouchDown(KeyboardTouch keyboardTouch);
    public abstract bool IsTouchUp(KeyboardTouch keyboardTouch);

    public abstract KeyboardPlatform GetRepresentedPlatform();
    public KeyboardTouch[] GetPressedTouches()
    {
        return m_activeKeys.ToArray();
    }
}
