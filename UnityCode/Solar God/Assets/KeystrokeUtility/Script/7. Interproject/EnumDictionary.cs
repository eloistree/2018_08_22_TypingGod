﻿
using System;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class EnumDictionary<T> where T : struct, IConvertible
{
    public List<T> m_activeElements = new List<T>();
    public Dictionary<T, bool> m_elementStates = new Dictionary<T, bool>();
    public delegate void OnStateChange(T element, bool isOn);
    public OnStateChange onStateChange;

    public EnumDictionary()
    {
        List<T> values = GetEnumList<T>();
        for (int i = 0; i < values.Count; i++)
        {
            if (!m_elementStates.ContainsKey(values[i]))
                m_elementStates.Add(values[i], false);
        }

    }
    public static List<G> GetEnumList<G>() where G : struct, IConvertible
    {
        return Enum.GetValues(typeof(G)).OfType<G>().ToList();
    }

    public void SetState(T element, bool value)
    {
        CheckExisting(element);
        if (value && m_elementStates[element] == false)
        {
            m_activeElements.Add(element);
            m_elementStates[element] = true;
            if (onStateChange != null)
                onStateChange(element, true);
        }
        else if (!value && m_elementStates[element] == true)
        {
            m_activeElements.Remove(element);
            m_elementStates[element] = false;
            if (onStateChange != null)
                onStateChange(element, false);
        }

    }

    private void CheckExisting(T element)
    {
        if (!m_elementStates.ContainsKey(element)) {
            m_elementStates.Add(element, false);
        }
    }

    public bool GetState(T element)
    {
        CheckExisting(element);
        return m_elementStates[element];
    }

    internal List<T> GetActiveElements()
    {
        return m_activeElements;
    }

    internal void Clear()
    {
        m_elementStates.Clear();
        m_activeElements.Clear();
    }
}
