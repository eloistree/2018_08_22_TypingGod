﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlocNote : MonoBehaviour {

    public string m_title;
    [Header("What is the note about ?")]
    [TextArea(2, 4)]
    public string m_subject;
    [TextArea(5,15)]
    public string m_blocNote;
    public bool m_affectingName;
    public void OnValidate()
    {
        if (m_affectingName) 
        this.gameObject.name = "Note: " + m_title;
    }
}
