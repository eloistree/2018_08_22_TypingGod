﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetNetworkAsTransmetterAtStart : MonoBehaviour {
    public bool m_atAwake=true;
    public bool m_atStart;
    public bool m_onEnable;

    void Awake()
    {
        if (m_atAwake)
            KeystrokeUtility.Network.SwitchTransmissionTo(TransmissionType.Transmitter);
    }
    void Start()
    {
        if (m_atStart)
            KeystrokeUtility.Network.SwitchTransmissionTo(TransmissionType.Transmitter);
    }

    void OnEnable () {
        if (m_onEnable)
            KeystrokeUtility.Network.SwitchTransmissionTo(TransmissionType.Transmitter);


    }
}
