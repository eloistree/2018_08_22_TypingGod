﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkKeyboardState : NetworkBehaviour {

    [SyncVar]
    public string m_keyboardState;
    public KeyboardState m_trackedKeyboard;
    public KeyboardStorageState m_networkRepresentation = new KeyboardStorageState();
    public float m_refreshDelay =0.1f;
	IEnumerator Start () {

        string state = "";
        while (true) {

        yield return new WaitForSeconds(m_refreshDelay<=0f?0.01f: m_refreshDelay);
            //   Debug.Log("K:" + (m_trackedKeyboard != null));
           state = GetCompressedKeyboard(m_trackedKeyboard);
        CheckForKeyboard();
            if(hasAuthority && isLocalPlayer)
                CmdSetKeyboardTo(state);

        }


    }

    private void CheckForKeyboard()
    {
        if (null != KeystrokeUtility.Keyboards.GetRepresentativeKeyboard())
            m_trackedKeyboard = KeystrokeUtility.Keyboards.GetRepresentativeKeyboard();

        if (m_trackedKeyboard == null)
        {
            m_trackedKeyboard = KeystrokeUtility.Keyboards.GetKeyboardBasedOnPlatform(Application.platform);
        }
        if (m_trackedKeyboard == null)
        {
            m_trackedKeyboard = KeystrokeUtility.Keyboards.GetKeyboardBasedOnPlatform(KeyboardPlatform.DefaultUnity);
        }
        if (m_trackedKeyboard == null)
        {
            m_trackedKeyboard = KeystrokeUtility.Keyboards.GetKeyboardBasedOnPlatform(KeyboardPlatform.Unknow);
        }
        if (m_trackedKeyboard != null && m_trackedKeyboard!= m_networkRepresentation) {
            m_networkRepresentation = new KeyboardStorageState() { m_representedPlatform = m_trackedKeyboard.GetRepresentedPlatform() };
        }
    }

    [Command]
    private void CmdSetKeyboardTo(string state)
    {
       // Debug.Log("cmd K:" +(m_trackedKeyboard != null));
        m_keyboardState = state;
        RpcRefreshInfo(state);
    }

    [ClientRpc]
    private void RpcRefreshInfo(string state) {

        GetKeyboardFromCompressed(state);
    }
    

    public string GetCompressedKeyboard(KeyboardState keyboard)
    {
        if (keyboard == null)
            return "";

        string compressed = "";
        compressed += keyboard.IsCapsLockOn()?"1|":"0|";
        compressed += keyboard.IsNumLockOn() ? "1|" : "0|"; ;
        compressed += keyboard.IsScrollLockOn() ? "1|" : "0|"; ;
        KeyboardTouch[] touches = keyboard.GetPressedTouches();
        for (int i = 0; i < touches.Length; i++)
        {
            compressed += ((int) touches[i] ) +(touches.Length-1==i?"": "|");
        }
        return compressed;
        
    }
    public KeyboardState GetKeyboardFromCompressed(string compressed)
    {
        m_networkRepresentation.Reset();
        string[] tokkens = compressed.Split('|');
        if (tokkens.Length < 3)
            return m_networkRepresentation;
        m_networkRepresentation.m_capsLockState = tokkens[0]=="1";
        m_networkRepresentation.m_numLockState = tokkens[1] == "1"; 
        m_networkRepresentation.m_scrollLockState = tokkens[2] == "1";

       // Debug.Log("Hey mon ami: " + tokkens[0] + "--> " + m_networkRepresentation.m_capsLockState);
        for (int i = 3; i < tokkens.Length; i++)
        {
            if(!string.IsNullOrEmpty(tokkens[i]))
                m_networkRepresentation.RealPressDown((KeyboardTouch)int.Parse(tokkens[i]));
        }
        return m_networkRepresentation;
    }
}
