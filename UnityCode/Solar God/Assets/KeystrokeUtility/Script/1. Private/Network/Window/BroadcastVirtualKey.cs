﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

using WindowsInput.Native;

public class BroadcastVirtualKey : NetworkBehaviour {


    [SerializeField]
    public VirtualKeyUnityEvent m_onReceivedDownEvent;
    [SerializeField]
    public VirtualKeyUnityEvent m_onReceivedUpEvent;


    public enum PushType : int { Down, Up }

    public IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);

            string[] names = NetworkUsersName.GetOthers();
            Debug.Log("Other: " + GetStringOf(names));
            names = NetworkUsersName.GetUserNames().ToArray();
            Debug.Log("All: " + GetStringOf(names));
            Debug.Log("Mine: " + NetworkUsersName.GetMine());
        }

    }

    public void PushVirtualKeyUpToOthers(KeyboardTouch key)
    {
        string[] names = NetworkUsersName.GetOthers();
        //Debug.Log("Other: " + GetStringOf(names));
        PushVirtualKeyUp(key, names);
    }

        public void PushVirtualKeyUp(KeyboardTouch key ,  string[] targetUsers)
    {

        if(isLocalPlayer && hasAuthority)
            CmdPushVirtualKey((int)key, (int) PushType.Up, targetUsers);
    }


    public void PushVirtualKeyDownToOthers(KeyboardTouch key)
    {
        string[] names = NetworkUsersName.GetOthers();
        //Debug.Log("Other: " + GetStringOf(names));
        PushVirtualKeyDown(key, names);
    }
    public void PushVirtualKeyDown(KeyboardTouch key ,  string[] targetUsers)
    {

        if (isLocalPlayer && hasAuthority)
            CmdPushVirtualKey((int)key,(int) PushType.Down, targetUsers);

    }

    [Command]
    private void CmdPushVirtualKey(int key, int type, string[] targetUsers) {

       // Debug.Log("Cmd Targets: " + GetStringOf(targetUsers));
        RpcPushVirtualKey(key, type, targetUsers);

    }

    [ClientRpc]
    private void RpcPushVirtualKey(int key, int type, string[] targetUsers)
    {

       // Debug.Log("Rpc Targets: " + GetStringOf(targetUsers));
        string name = NetworkUsersName.GetMine();
        bool isAllow = IsContainMyName(name, targetUsers);
        Debug.Log("Allow ? " + isAllow + "    " + name+ "  l: " + GetStringOf(targetUsers)  );
        if (!isAllow) return;
        Debug.Log("Network: " + ((PushType ) type )+ " " + ((KeyboardTouch)key));
        if (type == (int)PushType.Down && m_onReceivedUpEvent!=null)
            m_onReceivedDownEvent.Invoke((KeyboardTouch)key);
        if (type == (int)PushType.Up && m_onReceivedDownEvent != null)
            m_onReceivedUpEvent.Invoke((KeyboardTouch)key);


    }

    private string GetStringOf(string[] targetUsers)
    {
        string append = "";
        for (int i = 0; i < targetUsers.Length; i++)
        {
            append += targetUsers[i];
        }
        return append;
    }

    private bool IsContainMyName(string name, string[] targetUsers)
    {
        for (int i = 0; i < targetUsers.Length; i++)
        {
            if (name == targetUsers[i])
                return true;
        }
        return false;
    }
}
