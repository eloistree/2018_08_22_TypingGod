﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using WindowsInput.Native;

public class VirtualKeyboardTransmitter : MonoBehaviour , IPointerUpHandler, IPointerDownHandler
{

    public KeyboardTouch m_keyTransmitted;
    [System.Serializable]
    public class OnTransmitEvent : UnityEvent<KeyboardTouch> { }
    public OnTransmitEvent m_downEvent;
    public OnTransmitEvent m_upEvent;

    [Header("Debug (Don't touch)")]
    public MechnicalActionType m_last;
    public void Down()
    {
        m_downEvent.Invoke(m_keyTransmitted);
        m_last = MechnicalActionType.Down;
    }
    public void Up()
    {
        m_upEvent.Invoke(m_keyTransmitted);
        m_last = MechnicalActionType.Up;
    }
    public void Pressed() {
        Down();
        Up();
        m_last = MechnicalActionType.Pressed;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Up();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Down();
    }
}
