﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkUsersName : NetworkBehaviour {

    private static string GetStoreUserName() {
        return PlayerPrefs.GetString("#NetworkUserName#");

    }
    public static void SetStoreUserName(string name) {
        PlayerPrefs.SetString("#NetworkUserName#", name);
    }

    [SyncVar]
    public string m_userName;
    [SyncVar]
    public string m_deviceId;


    void Start () {
        string name = GetStoreUserName();
        if (string.IsNullOrEmpty(name)) {
            
            string date = DateTime.Now.ToString("HH:mm:ss.fff");
            name = SystemInfo.deviceName + ":" + date;
            SetStoreUserName(name);
        }
        ChangeUserName(name);
    }

	void ChangeUserName (string name) {
        if (isLocalPlayer && hasAuthority) {
            CmdChangeUserName(name);
            CmdChangeUserDeviceId(SystemInfo.deviceUniqueIdentifier);

        }

    }

    [Command]
    public void CmdChangeUserName(string name)
    {
        m_userName = name;
     }
    [Command]
    public void CmdChangeUserDeviceId(string id)
    {
        m_deviceId = id;
    }

    internal static string GetMine()
    {
        IEnumerable<NetworkUsersName> users = FindObjectsOfType<NetworkUsersName>().Where(t => t.isLocalPlayer && t.hasAuthority);
        if (users.Count() > 0)

            return users.First().m_userName;
        else
            return null;
    }

    internal static List<string> GetUserNames()
    {
        return FindObjectsOfType<NetworkUsersName>().Select(t => t.m_userName).ToList();
    }
    internal static List<string> GetDevicesId()
    {
        return FindObjectsOfType<NetworkUsersName>().Select(t => t.m_deviceId).ToList();
    }

    internal static string[] GetOthers()
    {

            List<string> names = GetUserNames();
            names.Remove(GetMine());
        return names.ToArray();
        
    }

    public string GetDeviceId()
    {
        return m_deviceId;
    }

    public static NetworkUsersName[] GetAll()
    {
        return FindObjectsOfType<NetworkUsersName>();
    }
    public static NetworkUsersName GetUserFromName(string name)
    {
        IEnumerable<NetworkUsersName> names =FindObjectsOfType<NetworkUsersName>().Where(k => k.name == name);
        if (names.Count() > 0)
            return names.First();
        return null;
    }
}
