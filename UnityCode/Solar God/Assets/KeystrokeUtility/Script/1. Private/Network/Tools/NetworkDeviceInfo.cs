﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkDeviceInfo : NetworkBehaviour
{
    

    [SyncVar]
    public string m_deviceId;

    [SyncVar]
    public string m_ipv4;

    [SyncVar]
    public string m_platform;

    void Start()
    {
        RefreshInfo();
    }

    void RefreshInfo()
    {
        if (isLocalPlayer && hasAuthority)
        {
            CmdSetDeviceId(SystemInfo.deviceUniqueIdentifier);
            CmdSetPlatform(Application.platform);
            //CmdSetIpv4(Network.player.ipAddress);

        }

    }
    [Command]
    private void CmdSetIpv4(string ipAddress)
    {
        m_ipv4 = ipAddress;
    }

    [Command]
    private void CmdSetPlatform(RuntimePlatform platform)
    {
        m_platform =""+ platform;
    }

    [Command]
    private void CmdSetDeviceId(string deviceUniqueIdentifier)
    {
        m_deviceId = deviceUniqueIdentifier;
    }
    
    
}