﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAndroidAsTransmetterAllTime : MonoBehaviour {
    
	IEnumerator Start () {
        
        while (Application.platform==RuntimePlatform.Android ) {
            KeystrokeUtility.Network.SwitchTransmissionTo(TransmissionType.Transmitter);
            yield return new WaitForSeconds(1f);

        }

        yield break;
	}
	
}
