﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput.Native;

public class UI_KeyStrokeUtilityDebug : MonoBehaviour {

    public InputFieldPlus m_inputNativeCharacter;
    public InputFieldPlus m_inputNativeUnityKeyCode;
    public InputFieldPlus m_inputNativeWindowKeyCode;

    public InputFieldPlus m_inputPressingTouch;
    public InputFieldPlus m_inputPressingCharacter;
    public InputFieldPlus m_inputStrokeChar;
    public InputFieldPlus m_inputStrokeText;
    public InputFieldPlus m_inputNetworkReceived;




    // Use this for initialization
    void Start()
    {
        KeystrokeUtility.InvokeDefaultListener();
        KeystrokeUtility.onNativeCharacter += WriteNativeChar;
        KeystrokeUtility.onNativeText += WriteNativeText;
        KeystrokeUtility.onNativeUnityKeyCode += WriteNativeKeyCode;
        KeystrokeUtility.onNativeWindowKeyCode += WriteNativeWindow;


        KeystrokeUtility.onKeyDown += WriteKeyDown;
        KeystrokeUtility.onKeyUp += WriteKeyUp;
        KeystrokeUtility.onStrokeRequestText += WriteText;
        KeystrokeUtility.onStrokeRequestCharacter += WriteChar;
        KeystrokeUtility.onStrokeRequestASCII += WriteASCII;
        KeystrokeUtility.onStrokeRequestUnicode += WriteUnicode;

        KeystrokeUtility.Network.onKeyDown += NetworkPressed;
        KeystrokeUtility.Network.onKeyUp += NetworkPressed;
        KeystrokeUtility.Network.onStrokeRequestCharacter += NetworkCharStroke;
        KeystrokeUtility.Network.onStrokeRequestText += NetworkTextStroke;


    }
    void OnDestroy()
    {
        KeystrokeUtility.onNativeCharacter -= WriteNativeChar;
        KeystrokeUtility.onNativeText -= WriteNativeText;
        KeystrokeUtility.onNativeUnityKeyCode -= WriteNativeKeyCode;
        KeystrokeUtility.onNativeWindowKeyCode -= WriteNativeWindow;


        KeystrokeUtility.onKeyDown -= WriteKeyDown;
        KeystrokeUtility.onKeyUp -= WriteKeyUp;
        KeystrokeUtility.onStrokeRequestText -= WriteText;
        KeystrokeUtility.onStrokeRequestCharacter -= WriteChar;
        KeystrokeUtility.onStrokeRequestASCII -= WriteASCII;
        KeystrokeUtility.onStrokeRequestUnicode -= WriteUnicode;

        KeystrokeUtility.Network.onKeyDown -= NetworkPressed;
        KeystrokeUtility.Network.onKeyUp -= NetworkPressed;
        KeystrokeUtility.Network.onStrokeRequestCharacter -= NetworkCharStroke;
        KeystrokeUtility.Network.onStrokeRequestText -= NetworkTextStroke;


    }

    private void NetworkTextStroke(NetworkTransmissionInfo info, TextStrokeRequest text)
    {

        m_inputNetworkReceived.ApprehendAtStart("NText: " + text.m_text + " ", true);
        CheckAndFlush(m_inputNetworkReceived);
    }


    private void NetworkCharStroke(NetworkTransmissionInfo info, CharacterStrokeRequest character)
    {
        m_inputNetworkReceived.ApprehendAtStart("NChar: " + character.m_character + " ", true);
        CheckAndFlush(m_inputNetworkReceived);

    }

    private void NetworkPressed(NetworkTransmissionInfo info, KeyboardTouchPressRequest touch)
    {
        m_inputNetworkReceived.ApprehendAtStart("NPress: " + touch.m_touch + ""+touch.m_pression+")", true);
        CheckAndFlush(m_inputNetworkReceived);

    }

    private void WriteNativeText(string value)
    {
        m_inputNativeCharacter.ApprehendAtStart("T: "+value+" ",true);

        CheckAndFlush(m_inputNativeCharacter);
    }

    private void WriteNativeChar(char value)
    {
        m_inputNativeCharacter.ApprehendAtStart("C: " + value + " ", true);

        CheckAndFlush(m_inputNativeCharacter);
    }

    private void WriteNativeKeyCode(KeyCode value, bool isDown)
    {
        m_inputNativeUnityKeyCode.ApprehendAtStart(""+value.ToString()+" (" + isDown + ")" + " ", true);

        CheckAndFlush(m_inputNativeUnityKeyCode);
    }

    private void WriteNativeWindow(VirtualKeyCode value, bool isDown)
    {
        m_inputNativeWindowKeyCode.ApprehendAtStart("" + value.ToString() + " (" + isDown + ")" + " ", true);

        CheckAndFlush(m_inputNativeWindowKeyCode);
    }

    private void WriteUnicode(UnicodeStrokeRequest codeNumber)
    {
        m_inputStrokeChar.ApprehendAtStart("U:"+KeyBindingTable.GetCharUnicode (codeNumber.m_code)
            +" (" + codeNumber.m_code+")" + " ", true);

        CheckAndFlush(m_inputStrokeChar);
    }

    private void WriteASCII(AsciiStrokeRequest codeNumber)
    {
        m_inputStrokeChar.ApprehendAtStart("A:" + KeyBindingTable.GetCharASCII(codeNumber.m_code)
       + " (" + codeNumber.m_code + ")" + " ", true);

        CheckAndFlush(m_inputStrokeChar);
    }

    private void WriteChar(CharacterStrokeRequest character)
    {
        m_inputStrokeChar.ApprehendAtStart("C:"+character.m_character + " ", true);
        CheckAndFlush(m_inputStrokeChar);
    }

    private void WriteText(TextStrokeRequest text)
    {
        m_inputStrokeText.ApprehendAtStart(text.m_text + " ", true);
        CheckAndFlush(m_inputStrokeText);
    }

    private void WriteKeyUp(KeyboardTouchPressRequest touch)
    {
        m_inputPressingTouch.ApprehendAtStart(touch.m_touch + " (Up)" + " ", true);
        CheckAndFlush(m_inputPressingTouch);
    }

    private void WriteKeyDown(KeyboardTouchPressRequest touch)
    {
        m_inputPressingTouch.ApprehendAtStart(touch.m_touch + " (Down)" + " ", true);
        CheckAndFlush(m_inputPressingTouch);
    }
    private void CheckAndFlush(InputFieldPlus input)
    {
        if (input.Length > 500)
            input.Clear();
    }

}
