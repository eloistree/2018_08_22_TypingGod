﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WindowsInput.Native;

public class Demo_TypeWithKeystrokeUtility : MonoBehaviour {

    [Header("Timing")]
    public float m_delayBeforeWriting=4;

    [Header("Touches")]
    public KeyboardTouch[] m_keyCodes = new KeyboardTouch[] { KeyboardTouch.O , KeyboardTouch.K };


    [Header("Charater")]
    public char[] m_characters=new char[] { '☢', '☣', '☠', '⚠', '®' , '¥' };




    [Header("Text")]
    [TextArea(2,10)]
    public string m_text = "(ಠ_ಠ)   (ノಠ益ಠ)ノ彡┻━┻       ┬──┬ ノ( ゜-゜ノ) ";



    [Header("Unicode")]
    public int m_numberOfUnicodeToDisplay = 100;


    [Header("Info ASCII")]
    public string[] m_asciiInfo;



    void Start () {
        Refresh();
        StartWritingAll();

    }

    private void StartWritingAll()
    {
       StartCoroutine(  WritingAllCoroutine());
    }

    private IEnumerator WritingAllCoroutine()
    {
        Debug.Log("Start...");
        yield return new WaitForSeconds(m_delayBeforeWriting);
        KeystrokeUtility.StrokeCharacter('\n');
        Debug.Log("Keyboard Touches... ");
        KeystrokeUtility.StrokeCharacter('\n');
        foreach (KeyboardTouch touch in m_keyCodes)
        {
            KeystrokeUtility.PressKeyDown(touch);
            KeystrokeUtility.PressKeyUp(touch);
        }
        KeystrokeUtility.StrokeCharacter('\n');
        Debug.Log("Characters... ");
        KeystrokeUtility.StrokeCharacter('\n');
        foreach (char c in m_characters)
        {
            KeystrokeUtility.StrokeCharacter(c);
        }
        KeystrokeUtility.StrokeCharacter('\n');
        Debug.Log("Text... ");
        KeystrokeUtility.StrokeCharacter('\n');
        KeystrokeUtility.StrokeText(m_text);


        KeystrokeUtility.StrokeCharacter('\n');
        Debug.Log("Write ASCII... ");
        KeystrokeUtility.StrokeCharacter('\n');
        for (int i = 0; i < 256; i++) {
            KeyBindingTable.InfoASCII info = KeyBindingTable.GetInfoASCII(i);
            char c = KeyBindingTable.GetCharASCII(i);
            KeystrokeUtility.StrokeText(i+": " +info.m_character);
            KeystrokeUtility.StrokeCharacter('\n');
        }

        KeystrokeUtility.StrokeCharacter('\n');
        Debug.Log("Write Unicode...");
        KeystrokeUtility.StrokeCharacter('\n');
        for (int i = 0; i < m_numberOfUnicodeToDisplay; i++)
        {

            KeystrokeUtility.StrokeText("   "+i+": ");
            KeystrokeUtility.StrokeUnicode(i);
        }
        Debug.Log("End Writing");
    }

    public void OnValidate()
    {
        Refresh();
    }

    public void Refresh() {
        m_asciiInfo = new string[256];
        for (int i = 0; i < 256; i++)
        {
            KeyBindingTable.InfoASCII ascii = KeyBindingTable.GetInfoASCII(i);
            m_asciiInfo[i] = string.Format("{0}: {1} ({2} - {3})", i, ascii.m_character, ascii.m_charDescription, ascii.m_name);
          
        }

    }
}
