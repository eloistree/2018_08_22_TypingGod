﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordBindKeyToDebugView : MonoBehaviour {

    public RecordBindingKey m_recordBinding;

    public Text m_text;
    public InputField m_field;
	void Start () {
		
	}
	
	public void Refresh ()
    {
        if (m_text)
            m_text.text = MacroCompressor.GetCompressionOf(m_recordBinding.m_macroToReplay);
        if (m_field)
            m_field.text = MacroCompressor.GetCompressionOf(m_recordBinding.m_macroToReplay);
    }
}
