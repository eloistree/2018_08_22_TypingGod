﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class MacroStorage : MonoBehaviour {

    private static string m_storagePath;
    public static string StorageDirectoryPath { get {
            if (m_storagePath == null)
                m_storagePath = Application.dataPath + "/MacroStorage/";
            return m_storagePath; }
    
    }

    public static void Save(string fileName, Macro macro) {
      string macroCompressed =  MacroCompressor.GetCompressionOf(macro);
        File.WriteAllText(StorageDirectoryPath+"/"+ fileName, macroCompressed);

    }

    public static string[] GetMacroFilesPath() {
      return Directory.GetFiles(StorageDirectoryPath);
    }
    public static string[] GetMacroNames()
    {
        string[] paths = GetMacroFilesPath();
        string[] names = new string[paths.Length];
        for (int i = 0; i < paths[i].Length; i++)
        {
            MacroInfo info = MacroCompressor.GetMacroInfo(paths[i]);
            names[i] = info.m_macroName;
        }
        return names;

    }

    public static Macro LoadFromFileName(string fileName)
    {
        string macroCompressed = File.ReadAllText(StorageDirectoryPath+"/"+fileName);
        return MacroCompressor.LoadFrom(macroCompressed);
    }
    public static Macro LoadFromName(string macroName)
    {
        throw new System.NotImplementedException();
    }

    public static void AddAsOftenUsed(string macroName)
    {

    }

    public static void RemoveAsOftenUsed(string macroName)
    {

    }
}
