﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using WindowsInput;
using WindowsInput.Native;

public class MacroHolder : MonoBehaviour {

    [SerializeField]
    public Macro m_macro ;
    public TextAsset m_macroText;

    //public void OnValidate()
    //{
    //    Debug.Log("Hey Mon ami");
    //    m_jsonExporter = JsonUtility.ToJson(m_macro);
    //}


    public void Start()
    {
        ImportMacro();
    }
    public void OnValidate()
    {

        ImportMacro();
    }

    private void ImportMacro()
    {
        try
        {
            if (m_macroText != null)
                m_macro = MacroCompressor.LoadFrom(m_macroText.text);
            else m_macro = new Macro();
        }

        catch
        {
            Debug.LogError("Impossible to convert as Macro: " + m_macroText.name + "\n" + m_macroText.text);
        }
    }

}

[System.Serializable]
public class VirtualKeyUnityEvent : UnityEvent<KeyboardTouch> { }
public delegate void  VirtualKeyEvent(KeyboardTouch keyCode);

[System.Serializable]
public class AbstractKeyEvents
{
    [SerializeField]
    public VirtualKeyUnityEvent m_onDownUnityEvent = new VirtualKeyUnityEvent();
    [SerializeField]
    public VirtualKeyUnityEvent m_onUpUnityEvent = new VirtualKeyUnityEvent();

    [SerializeField]
    public VirtualKeyEvent m_onDownEvent;
    [SerializeField]
    public VirtualKeyEvent m_onUpEvent;

    public void NotifyDown(KeyboardTouch key)
    {
        if (m_onDownEvent != null)
            m_onDownEvent(key);
        if (m_onDownUnityEvent != null)
            m_onDownUnityEvent.Invoke(key);
    }
    public void NotifyUp(KeyboardTouch key)
    {
        if (m_onUpEvent != null)
            m_onUpEvent(key);
        if (m_onUpUnityEvent != null)
            m_onUpUnityEvent.Invoke(key);
    }


}
public class MacroPlayer : MonoBehaviour {

    private static MacroPlayer m_instace;
    public static MacroPlayer InstanceInScene { get {
            if (m_instace == null) {
                GameObject obj = new GameObject("# MacroPlayer");
                MacroPlayer player = obj.AddComponent<MacroPlayer>();
                m_instace = player;
            }
            return m_instace;
        } }

    
    public static void Play(Macro macro, PlayType playType, AbstractKeyEvents macroCallBack) {
         InstanceInScene.PlayMacro( GetPlayCoroutine(macro, playType, macroCallBack));
    }

    public void PlayMacro(IEnumerator macroCoroutine) {
        StartCoroutine(macroCoroutine);
    }

    public enum PlayType { Window, Events, All}
    public static IEnumerator GetPlayCoroutine(Macro macro, PlayType playType, AbstractKeyEvents macroCallBack = null)
    {
        Debug.Log("Playing start:  " + macro.m_macroName + "  " + macro.m_actions.Count);
        InputSimulator simulator = new InputSimulator();

        bool onNativeWindow = false;
        bool canBeExecuteOnWindow = false;
        bool canSendEvents = false;
#if UNITY_STANDALONE_WIN || UNITY_EDITOR
        onNativeWindow = true;
#endif
        canBeExecuteOnWindow = onNativeWindow && (playType == PlayType.Window || playType == PlayType.All);
        canSendEvents = macroCallBack != null && (playType == PlayType.Events || playType == PlayType.All);
        //MacroAction[] actions = new MacroAction[macro.m_actions.Count];
        //macro.m_actions.CopyTo(actions);
        int lenght = macro.m_actions.Count;
        for (int i = 0; i < lenght; i++)
        {

            MacroAction action = macro.m_actions[i];
            //Debug.Log("Do -> " + action.m_description + " i:" + action.GetType());
            if (action is ActionWait)
            {
                ActionWait wait = (ActionWait)action;
                if (wait.m_milliseconds > 0f)
                {

                    //        Debug.Log("Wait -> " + wait.m_milliseconds);
                    yield return new WaitForSeconds(wait.m_milliseconds * 0.001f);
                }

            }
            
            if (action is ActionKeystroke)
            {
                //Debug.Log("1");
                ActionKeystroke strokeLetter = (ActionKeystroke)action;

                KeyboardTouch touch = strokeLetter.m_touch;
                MechnicalActionType mecAction = strokeLetter.m_mecanicalAction;
                if (mecAction == MechnicalActionType.Down || mecAction == MechnicalActionType.Pressed) {

                    KeystrokeUtility.PressKeyDown(touch);
                    if (canSendEvents)
                        macroCallBack.NotifyDown(strokeLetter.m_touch);

                }
                if (mecAction == MechnicalActionType.Up || mecAction == MechnicalActionType.Pressed) {

                    KeystrokeUtility.PressKeyUp(touch);
                    if (canSendEvents)
                        macroCallBack.NotifyUp(strokeLetter.m_touch);
                }

            }


            if (action is ActionMoveToPrimaryScreenPosition)
            {

                ActionMoveToPrimaryScreenPosition move = (ActionMoveToPrimaryScreenPosition)action;
                MovePointerTo(simulator, move.m_xPourcent, move.m_yPourcent);
            }

            if (action is ActionClick)
            {

                ActionClick click = (ActionClick)action;
                if (click.m_mouseAction == ActionClick.ActionType.Left)
                {

                    switch (click.m_mecanicalAction)
                    {
                        case MechnicalActionType.Down:
                            simulator.Mouse.LeftButtonDown(); break;
                        case MechnicalActionType.Up:
                            simulator.Mouse.LeftButtonUp(); break;
                        case MechnicalActionType.Pressed:
                            simulator.Mouse.LeftButtonClick();
                            break;
                        default:
                            break;
                    }
                }
                if (click.m_mouseAction == ActionClick.ActionType.Right)
                {

                    switch (click.m_mecanicalAction)
                    {
                        case MechnicalActionType.Down:
                            simulator.Mouse.RightButtonDown(); break;
                        case MechnicalActionType.Up:
                            simulator.Mouse.RightButtonUp(); break;
                        case MechnicalActionType.Pressed:
                            simulator.Mouse.RightButtonClick();
                            break;
                        default:
                            break;
                    }
                }
            }

            if (action is ActionDoubleClick)
            {

                ActionDoubleClick click = (ActionDoubleClick)action;
                if (click.m_mouseAction == ActionDoubleClick.ActionType.Left)
                    simulator.Mouse.LeftButtonDoubleClick();
                if (click.m_mouseAction == ActionDoubleClick.ActionType.Right)
                    simulator.Mouse.RightButtonDoubleClick();
            }
        }

        Debug.Log("Playing Stop");

    }

    


    private static void MovePointerTo(InputSimulator simulator, double x, double y)
    {
        Debug.Log(Screen.currentResolution.width + " x " + Screen.currentResolution.height);
        Debug.Log(x * Screen.currentResolution.width + " x " + y * Screen.currentResolution.height);
        x = x * Screen.currentResolution.width * 65535 / Screen.currentResolution.width;
        y = y * Screen.currentResolution.height * 65535 / Screen.currentResolution.height;
        simulator.Mouse.MoveMouseTo(x, y);
    }

    internal static void PlayKeyDown(KeyboardTouch key, PlayType type, AbstractKeyEvents callback = null)
    {
        Macro toPlay = new Macro();
        toPlay.AddKeyCodeDown(key);
         Play(toPlay, type, callback);
    }
    internal static void PlayKeyUp(KeyboardTouch key, PlayType type, AbstractKeyEvents callback = null)
    {
        Macro toPlay = new Macro();
        toPlay.AddKeyCodeUp(key);
        Play(toPlay, type, callback);
    }
}
public class MacroInfo
{

    public string m_macroName;
}



public class Macro {
    public string m_macroName;
    public List<MacroAction> m_actions = new List<MacroAction>();

    public Macro()
    {
        
    }
    

    public void AddWait(float millisecond)
    {
        m_actions.Add(new ActionWait(millisecond));
    }
    
    public void AddKeyCodeUp(KeyboardTouch key)
    {
        m_actions.Add(new ActionKeystroke(key, MechnicalActionType.Up));
    }



    public void AddKeyCodeDown(KeyboardTouch key)
    {
        m_actions.Add(new ActionKeystroke(key, MechnicalActionType.Down));
    }
    public void AddKeyCodePressed(KeyboardTouch key)
    {
        m_actions.Add(new ActionKeystroke(key, MechnicalActionType.Pressed));
    }

    public void AddMouseDeplacement(float xPct, float yPct) {
        m_actions.Add(new ActionMoveToPrimaryScreenPosition(xPct, yPct));
    }

    public void AddClickDown(ActionClick.ActionType action) { m_actions.Add(new ActionClick(action, MechnicalActionType.Down)); }
    public void AddClickUp(ActionClick.ActionType action) { m_actions.Add(new ActionClick(action, MechnicalActionType.Up)); }
    public void AddClickPressed(ActionClick.ActionType action) { m_actions.Add(new ActionClick(action, MechnicalActionType.Pressed)); }
    public void AddClickDouble(ActionDoubleClick.ActionType action) { m_actions.Add(new ActionDoubleClick(action)); }

    internal void Clear()
    {
        m_actions.Clear();
    }

    internal string GetName()
    {
        return m_macroName;
    }

    internal void AddCharacterStroke(char character)
    {
        m_actions.Add(new ActionWriteCharacter(character));
    }

    internal void AddTextStroke(string text)
    {

        m_actions.Add(new ActionWriteText(text));
    }
}

[System.Serializable]
public class MacroAction
{
    public string m_description;
    public string m_short = "\n";
}


[System.Serializable]
public class ActionWait:MacroAction
{
    public float m_milliseconds = 0f;

    public ActionWait(float milliseconds) {
        m_milliseconds = milliseconds;
        m_description = "Wait " + milliseconds;
        m_short = MacroCompressor.GetCompressionOf(this);
    }
    
}

/// <summary>
/// Down = start to press, up stop to press, press = down and up
/// </summary>
public enum MechnicalActionType { Down, Pressed, Up };
//[System.Serializable]
//public class ActionVirtualLetter : MacroAction
//{
//    public WindowsInput.Native.VirtualKeyCode m_virtualKeyCode;
//    public MechnicalActionType m_mecanicalAction;

//    public ActionVirtualLetter(VirtualKeyCode key, MechnicalActionType mecanicalAction)
//    {
//        m_description = mecanicalAction + " " + key;
//        m_virtualKeyCode = key;
//        m_mecanicalAction = mecanicalAction;
//        m_short = MacroCompressor.GetCompressionOf(this);


//    }
//}
[System.Serializable]
public class ActionKeystroke : MacroAction
{
    public KeyboardTouch m_touch;
    public MechnicalActionType m_mecanicalAction;

    public ActionKeystroke(KeyboardTouch key, MechnicalActionType mecanicalAction)
    {
        m_description = mecanicalAction + " " + key;
        m_touch = key;
        m_mecanicalAction = mecanicalAction;
        m_short = MacroCompressor.GetCompressionOf(this);


    }
}
[System.Serializable]
public class ActionClick : MacroAction
{
    public ActionType m_mouseAction;
    public MechnicalActionType m_mecanicalAction;

    public ActionClick(ActionType action, MechnicalActionType mechanicalAction)
    {
        m_description = mechanicalAction + " " + action;
        m_mouseAction = action;
        m_mecanicalAction = mechanicalAction;
        m_short = MacroCompressor.GetCompressionOf(this);

    }
    public enum ActionType:int  { Left, Right, Mid };

}


[System.Serializable]
public class ActionWriteCharacter : MacroAction
{
    public char m_character;

    public ActionWriteCharacter(char character)
    {
        m_character = character;
        m_description = "Write Character '" + m_character + "'";
        m_short = MacroCompressor.GetCompressionOf(this);
    }

}

[System.Serializable]
public class ActionWriteText : MacroAction
{
    public string m_text;

    public ActionWriteText(string text)
    {
        m_text = text;
        m_description = "Write Text (" + m_text.Length+")";
        m_short = MacroCompressor.GetCompressionOf(this);
    }

}


[System.Serializable]
public class ActionDoubleClick : MacroAction
{
    public ActionType m_mouseAction;

    public ActionDoubleClick(ActionType action)
    {
        m_description =  ""+ action;
        m_mouseAction = action;
        m_short = MacroCompressor.GetCompressionOf(this);
    }
    public enum ActionType:int { Left, Right };

}
[System.Serializable]
public class ActionMoveToPrimaryScreenPosition : MacroAction
{
    public float m_xPourcent;
    public float m_yPourcent;

    public ActionMoveToPrimaryScreenPosition(float xPct, float yPct)
    {
        m_description = "Move mouse to " + xPct+"x"+yPct;
        m_xPourcent = xPct;
        m_yPourcent = yPct;
        m_short = MacroCompressor.GetCompressionOf(this);
    }
}


public class MacroCompressor {

    public static string m_idActionWait = "aw";
    public static string m_idActionClick = "ac";
    public static string m_idActionDoubleClick = "acd";
    public static string m_idActionMoveToScreenPosition = "amsp";
    public static string m_idActionVirtualLetter = "avl";
    public static string m_idActionActractLetter = "aal";
    public static string m_idActionCharacter = "ach";
    public static string m_idActionText = "atxt";


    public static string GetWarpedShortenId(MacroAction action)
    {
        return WarpId(GetShortenId(action));
    }
    private static string WarpId(string id)
    {
        return "\n" + id + ":";
    }

    public static string GetShortenId(MacroAction action)
    {
        if (action is ActionWait) return m_idActionWait;
        if (action is ActionWriteCharacter) return m_idActionCharacter;
        if (action is ActionKeystroke) return m_idActionActractLetter;
        if (action is ActionClick) return m_idActionClick;
        if (action is ActionDoubleClick) return m_idActionDoubleClick;
        if (action is ActionMoveToPrimaryScreenPosition) return m_idActionMoveToScreenPosition;
        if (action is ActionWriteText) return m_idActionText;
        return "";
    }

    public static string GetCompressionOf(ActionClick action)
    {
        string shorten = GetWarpedShortenId(action) ;
        switch (action.m_mecanicalAction)
        {
            case MechnicalActionType.Down:
                shorten += "d";
                break;
            case MechnicalActionType.Pressed:
                shorten += "p";
                break;
            case MechnicalActionType.Up:
                shorten += "u";
                break;
        }
        shorten += (int)action.m_mouseAction;

        return shorten;
    }
    public static string GetCompressionOf(ActionDoubleClick action)
    {
        string shorten = GetWarpedShortenId(action);
        shorten += (int)action.m_mouseAction;
        return shorten;
    }

    public static string GetCompressionOf(ActionWait action)
    {
        string shorten = GetWarpedShortenId(action) + action.m_milliseconds;
        return shorten;
    }
    

    public static string GetCompressionOf(ActionKeystroke action)
    {
        string shorten = GetWarpedShortenId(action);
        switch (action.m_mecanicalAction)
        {
            case MechnicalActionType.Down:
                shorten += "d";
                break;
            case MechnicalActionType.Pressed:
                shorten += "p";
                break;
            case MechnicalActionType.Up:
                shorten += "u";
                break;
        }
        shorten += (int)action.m_touch;
        return shorten;
    }
    public static string GetCompressionOf(ActionMoveToPrimaryScreenPosition action)
    {
        string shorten = GetWarpedShortenId(action);
        shorten += action.m_xPourcent;
        shorten += ":";
        shorten += action.m_yPourcent;
        return shorten;

    }

    public static string GetCompressionOf(ActionWriteCharacter action)
    {
        string shorten = GetWarpedShortenId(action);
        shorten += action.m_character;
        return shorten;

    }

    public static string GetCompressionOf(ActionWriteText action)
    {
        string shorten = GetWarpedShortenId(action);
        shorten += action.m_text;
        return shorten;

    }


    public static string GetCompressionOf(MacroAction action)
    {
        if (action is ActionWait) return GetCompressionOf((ActionWait) action);
        if (action is ActionWriteCharacter) return GetCompressionOf((ActionWriteCharacter)action);
        if (action is ActionWriteText) return GetCompressionOf((ActionWriteText)action);
        if (action is ActionKeystroke) return GetCompressionOf((ActionKeystroke)action);
        if (action is ActionClick) return GetCompressionOf((ActionClick)action);
        if (action is ActionDoubleClick) return GetCompressionOf((ActionDoubleClick)action);
        if (action is ActionMoveToPrimaryScreenPosition) return GetCompressionOf((ActionMoveToPrimaryScreenPosition)action);
        return "";
    }

    public static  string GetCompressionOf(Macro macro)
    {
        string comp = "name:" + macro.m_macroName;
        for (int i = 0; i < macro.m_actions.Count; i++)
        {
            comp+= GetCompressionOf(macro.m_actions[i]);
        }
        return comp;

    }

    internal static Macro LoadFrom(string text)
    {
        Macro macro = new Macro();
        string[] lines = text.Split('\n');
        if (lines.Length <= 0) return macro;
        macro.m_macroName = lines[0].Substring("name:".Length);
        macro.m_actions.Clear();
        for (int i = 1; i < lines.Length; i++)
        {
           MacroAction action = CreateFromCompressed(lines[i]);
            if(action!=null)
            macro.m_actions.Add(action);
        }

        return macro;
    }

    private static MacroAction CreateFromCompressed(string line)
    {
        Type typeOf = GetTypeOf(line);
        if (typeOf == typeof(ActionWait))
            return CreateActionWaitFrom(line);
        if (typeOf == typeof(ActionKeystroke))
            return CreateActionAbstractKeyFrom(line);
        if (typeOf == typeof(ActionWriteCharacter))
            return CreateActionWriteCharacterFrom(line);
        if (typeOf == typeof(ActionWriteText))
            return CreateActionWriteTextFrom(line);
        return null;
    }

    private static MacroAction CreateActionWriteTextFrom(string line)
    {
        string content = line.Substring(GetWarpId(m_idActionText).Length - 1);
        try
        {
            return new ActionWriteText(content);
        }
        catch { return null; }
    }

    private static MacroAction CreateActionWriteCharacterFrom(string line)
    {

        string content = line.Substring(GetWarpId(m_idActionCharacter).Length - 1);
        

        try
        {
            char c = char.Parse(content);
            return new ActionWriteCharacter(c);
        }
        catch {
            Debug.LogWarning("Are you sure that it is a character ? (" + content + ")");
            return null; }
    }

    private static ActionWait CreateActionWaitFrom(string line)
    {
        string content = line.Substring(GetWarpId(m_idActionWait).Length-1);
        
        try {
            float millisecond = float.Parse(content);
            return new ActionWait(millisecond);
        }
        catch { return null; }
    }

    private static ActionKeystroke CreateActionAbstractKeyFrom(string line)
    {
        string content = line.Substring(GetWarpId(m_idActionActractLetter).Length - 1);

        char actionType = content[0];
        MechnicalActionType meca = MechnicalActionType.Down;
        if (actionType == 'u') meca = MechnicalActionType.Up;
        else if (actionType == 'p') meca = MechnicalActionType.Pressed;

        try
        {
            int key = int.Parse(content.Substring(1));
            return new ActionKeystroke((KeyboardTouch) key, meca);
        }
        catch { return null; }
    }

    private static Type GetTypeOf(string line)
    {

        if (StartWith(line, m_idActionWait)) return typeof(ActionWait);
        if (StartWith(line, m_idActionVirtualLetter)) return typeof(ActionKeystroke);
        if (StartWith(line, m_idActionCharacter)) return typeof(ActionWriteCharacter);
        if (StartWith(line, m_idActionText)) return typeof(ActionWriteText);

        return null;
    }

    private static bool StartWith(string line, string id)
    {
        string wid = GetWarpId(id);
        //Debug.Log("wid:  " + wid);
        return line.StartsWith(id);
    }
    public static string GetWarpId(string id) {

        string warpForm = "\n{0}:";
        return string.Format(warpForm, id);
    }

    internal static MacroInfo GetMacroInfo(string filePath)
    {

        MacroInfo info = new MacroInfo();
        StreamReader readingFile = new StreamReader(filePath);
        string readingLine = readingFile.ReadLine();
        readingLine = readingLine.Substring("name:".Length);
        info.m_macroName = readingLine;
        return info;
    }
}