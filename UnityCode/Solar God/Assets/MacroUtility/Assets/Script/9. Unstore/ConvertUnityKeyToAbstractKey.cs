﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConvertUnityKeyToAbstractKey : ConvertToAbstractKey
{


    public List<KeyboardTouch> m_keysPressed;


    public void UnityKeyDown(KeyCode key) {
        KeyboardTouch bindKey;
        bool isConvertable;
        KeyBindingTable.ConvertUnityKeyToTouch(key, out bindKey, out isConvertable);
        
        if (isConvertable)
        {
            m_keysPressed.Add(bindKey);
            NotifyKeyDown(bindKey);
        }

    }
    public void UnityKeyUp(KeyCode key) {
        KeyboardTouch bindKey;
        bool isConvertable;
        KeyBindingTable.ConvertUnityKeyToTouch(key, out bindKey, out isConvertable);
        if (isConvertable)
        {
            m_keysPressed.Remove(bindKey);
            NotifyKeyUp(bindKey);
        }
    }
}
