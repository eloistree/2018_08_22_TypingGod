﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using WindowsInput;

public class RecordVirtualKeyCode : MonoBehaviour {

    [System.Serializable]
    public class VirtualKeyEvent : UnityEvent<WindowsInput.Native.VirtualKeyCode> { }

    [SerializeField]
    public VirtualKeyEvent onKeyPressedEvent;
    [SerializeField]
    public VirtualKeyEvent onKeyReleasedEvent;


    public delegate void VirtualKeyDetected(WindowsInput.Native.VirtualKeyCode virtualKey);


    public VirtualKeyDetected onKeyPressed;
    public VirtualKeyDetected onKeyReleased;




    public Dictionary<WindowsInput.Native.VirtualKeyCode, bool> keysState ;


    public bool m_useDebug;


    void Awake ()
    {
        keysState = new Dictionary<WindowsInput.Native.VirtualKeyCode, bool>();
        foreach (WindowsInput.Native.VirtualKeyCode keycode in Enum.GetValues(typeof(WindowsInput.Native.VirtualKeyCode)))
         {
            if (!keysState.ContainsKey(keycode))
                keysState.Add(keycode, false);
        }

    }
	

	void Update () {

        InputSimulator simulator = new InputSimulator();
        foreach (WindowsInput.Native.VirtualKeyCode keycode in Enum.GetValues(typeof(WindowsInput.Native.VirtualKeyCode))) 
        {

            bool keyState = keysState[keycode];
            if (keyState && !simulator.InputDeviceState.IsKeyDown(keycode))
            {
                if(m_useDebug)
                    Debug.Log("Release " + keycode + "  "+(int) keycode);
                keysState[keycode] = false;
                onKeyPressedEvent.Invoke(keycode);
                if (onKeyPressed != null)
                    onKeyPressed(keycode);
            }
            else if(!keyState && simulator.InputDeviceState.IsKeyDown(keycode))
            {

                if (m_useDebug)
                    Debug.Log("Press " + keycode + "  " + (int)keycode);
                keysState[keycode] = true;
                onKeyReleasedEvent.Invoke(keycode);
                if(onKeyReleased!=null)
                onKeyReleased(keycode);

            }
        }
    }
    
}
