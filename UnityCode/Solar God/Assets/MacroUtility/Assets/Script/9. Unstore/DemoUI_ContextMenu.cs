﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoUI_ContextMenu : MonoBehaviour {

    public GameObject[] m_panels;

    public void DeactivateAll() {
        foreach (GameObject obj in m_panels)
        {
            obj.SetActive(false);
        }
    }
}
