﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using WindowsInput.Native;

public class MacroUtility
{
    public static List<Char> GetAllUnityCharacters() {

        List<Char> printableChars = new List<char>();
        for (int i = char.MinValue; i <= char.MaxValue; i++)
        {
            char c = Convert.ToChar(i);
            if (!char.IsControl(c))
            {
                printableChars.Add(c);
            }
        }
        return printableChars;
    }

    public static List<T> GetEnumList<T>() where T : struct, IConvertible
    {
        return Enum.GetValues(typeof(T)).OfType<T>().ToList();
    }
    public static List<string> GetEnumNamesList<T>()  where T : struct, IConvertible
    {
        return Enum.GetNames(typeof(T)).ToList() ;
    }
} 
